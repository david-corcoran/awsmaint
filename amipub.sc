-- comment
CREATE TABLE amiall(imageId, LPcnt int, Built, License, Type, Product, Release, Project, Build, Grade, BW, Size, HF, key, name
, constraint ami_u unique(imageId) on conflict ignore);
CREATE TABLE firstseen(tagged int,seen datetime, imageId, constraint img_u unique(imageId) on conflict ignore);
CREATE TABLE intake(refreshed datetime,isPublic int,imageId, architecture,imageState, name, constraint intake_u unique(imageId) on conflict replace);
CREATE TABLE launchperm( userId int, imageId, constraint user_img_u unique(userId, imageId) on conflict replace);
CREATE TABLE lpchktime(chktime datetime, imageId, constraint lpchktime_u unique(imageId) on conflict replace);
CREATE TABLE param(ival int, dval datetime, sval, key, constraint key_u unique(key) on conflict replace);
CREATE TRIGGER dereged after delete on intake
begin
  delete from lpchktime where imageId = old.imageId;
  delete from launchperm where imageId = old.imageId;
  delete from amiall where imageId = old.imageId;
end;
CREATE TRIGGER lpchkupd after insert on launchperm
begin
  insert into lpchktime(chktime, imageId)
  values (datetime('now','localtime'), new.imageId);
end;
CREATE TRIGGER trackcreate after insert on intake
begin
  insert into firstseen(seen, imageId)
  values (datetime('now','localtime'), new.imageId);
end;
