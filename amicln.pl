#!/usr/bin/perl
# Perform AWS cleanup maintenance

use strict;
use Data::Dumper;
use VM::EC2;
use YAML;
use feature qw(say);

$Data::Dumper::Indent = 1;
my $AWS = YAML::LoadFile('/root/.aws/awsinfo.yml');
my $EC2 = VM::EC2->new(-access_key=>$AWS->{AWS_ACCESS_KEY},-secret_key=>$AWS->{AWS_SECRET_KEY});
my @Instances =  $EC2->describe_instances(); # get all instances

exit main();

sub main
{
    # this order precludes delay due to state changes (I think)
    #
    deregister_amis(); # deregister is the term for deleting
    termniate_instances();
    stop_instances();
}

# deregister (delete) AMIs
#
sub deregister_amis
{
    my $rtn = 0;
    my @amis = $EC2->describe_images(-owner => $AWS->{AWS_MY_ACCOUNT_ID});
    my @aoi = grep {
       length($_->{data}{tagSet}{item}{Name}{value}) == 0  # no name
       and $_->{data}{name} !~ /tmos-tier2/                # tmos-tier2 AMIs are never deleted
       and (
           ($a = $_->{data}{tagSet}{item}{expiry}{value}) =~ m/\d{4}-\d{2}-\d{2}/
           and $a lt date_hence(0)
       )
    } @amis;
    
    for my $img (@aoi) {
        $rtn &&= $EC2->deregister_image($img);
    }

    return $rtn;
}

# terminate AMI instances over 30 days old
# 
sub terminate_instances
{
    my @ioi = grep { # ioi - instances of interest
        $_->{data}{keyName} eq $AWS->{AWS_KEYPAIR_NAME}
        and $_->{data}{instanceState}{name} eq 'stopped'
        and not defined($_->{data}{tagSet}{item}{Name}{value}) 
        and $_->{data}{launchTime} lt date_hence(-30)
    } @Instances;

    #D printf "%s %s\n", $_->{data}{instanceId}, $_->{data}{launchTime} for @ioi; return 0;

    my $j = 0;
    my $k = 0;

    for my $i (@ioi) {
        printf "%s %s\n",$i, $i->terminate();
        for my $b (@{$i->{data}{blockDeviceMapping}{item}}) {
            my $vol = $b->{ebs}{volumeId};
            if ($EC2->delete_volume($vol)) {
                printf "%s %s\n", $vol, 'deleted';
                $k++;
            }
        }
        $j++;
    }

    printf "Instances terminated: %d\n", $j;
    printf "Volumnes deleted: %d\n", $k;
    return 0;
}

sub stop_instances
{
    my @uinstances = grep {
        length($_->{data}{tagSet}{item}{Name}{value}) == 0
        and $_->{data}{instanceState}{name} eq 'running'
        and $_->{data}{launchTime} lt date_hence(-3)
        and $_->{data}{keyName} eq $AWS->{AWS_KEYPAIR_NAME}
    } @Instances;

    if (@uinstances) {
        say "Deleting:";
        for my $ui (@uinstances) {
            printf "%s %s %s %s\n"
            , $ui->{data}{instanceId}
            , $ui->{data}{launchTime}
            , $ui->{data}{instanceState}{name}
            , $ui->{data}{tagSet}{item}{Name}{value};
            $ui->stop();
        }
    } else {
        say "No old unnamed instances running" ;
    }
    return 0;
}


# generate datestamp give number of days plus or minus
#
sub date_hence
{
    my ($days) = @_;
    my $rtn;
    my @date = (localtime(time() + (3600*24*$days)))[5,4,3]; 
    $date[0]+=1900;
    $date[1]+=1;
    my $rtn = sprintf "%04d-%02d-%02d", @date; 
    return $rtn;
}

=pod

=head1 NAME

 amicln.pl - cleanup AWS of instances and AMIs

=head1 SYNOPSIS

 perl amicln.pl

=head1 DESCRIPTION

Cleans up AWS of unnamed running instances, stopped instances and old AMIs in the order shown below.

=over 2

=item Deregister AMIs

Deregister (delete) AMIs unless they:

  1. have expiry tag (see amitag.pl) which is populated with a future date

  2. have 'tmos-tier2' in the description (project name)

  3. a name associated with the AMI.

=item Terminate Instances

Terminate stop instances and associated volumes.

=item Stop Running Instances

Stop running instances that are unamed, have the CM keyname (purple-nurple) and
launched over 3 days ago. Unlike AMIs the instance dataset contains the
instance launch date.

=back

=head1 SEE ALSO

 amicsv.pl
 amidesc.pl
 amilist.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

  /root/.aws/awsinfo.yml - contains AWS keys to access CM's account

=cut
