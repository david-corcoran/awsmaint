# list amis
use strict;
use Data::Dumper;
use Sys::Hostname;
use CGI qw(:standard :html4 center start_table end_table);
use DBI;
use feature qw(say);

exit main();

# F5 Networks Hourly Hotfix-BIGIP-11.6.0.3.0.412-HF3 - Best 25Mbps - built on Feb 20 2015 10_10AM - size_104GB

sub main
{
    my $dbg = 1;
    open X,">x";
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});
    my $rev = '11.6';
    # printf "\nhttps://docs.f5net.com/display/PDVADCTEAM/Setup+Of+Different+Platforms+for+VE\n", $rev;

    my $sth;
# imageId, Built, License, Type, Product, Release, Project, Build, Grade, BW, Size, HF, key, name

    $sth = $dbh->prepare(qq{
        select Built, Project, imageId ID, Build, Grade, Product, Release, HF, Size, BW
        , name Description, Type, License, LPcnt LP
        from amiall
        order by built desc
    });

    my %row;

    # N.B. this definition and mapping would not be necessary if DBI (or maybe
    # it is DBD::SQLlite) on this system supported the NAME construct
    # https://metacpan.org/pod/DBI#NAME1
    #
    my @cols = qw(Built Project ID Build Grade Product Release HF Size BW
        Description Type License LP);
    $a = 0;
    my %cols = map {$_, $a++} @cols;

    my @dispcol = @cols{qw(
        Built ID LP License Type Product Project Release Build HF Grade BW Size
        Description
    )};

    #D say Dumper \%cols;

    $sth->bind_columns(\@row{@cols});
    $sth->execute();

    open HTM,"> amilist.html";

    print HTM start_html(
          -title => 'AMI List'
        , -style => [
              {-src => "/datatable.css"}
            , {-src => "amilist.css"}
        ]
        , -head => [
              meta({-http_equiv => 'Content-Type', -content => 'text/html', -charset => 'utf-8'})
            , meta( {-http_equiv => 'Cache-control', -content=>'NO-CACHE'} )
        ]
        , -script => [
              {-type => 'text/javascript', -src => "/js/jquery-1.11.0.min.js"}
            , {-type => 'text/javascript', -src => "/js/datatable.js"}
            , {-type => 'text/javascript', -code => q{
                $(document).ready(function() {
                    $('#main-table').datatable({
                        'pageSize': 9999,
                        'sort': [true, true, true, true, true, true, true, true
                            , true, true, true, true, true, true]
                        , 'pageSize':10
                        , 'filters': [true, true, 'select', 'select', 'select', 'select'
                        , 'select', 'select', 'select', 'select', 'select', 'select'
                        , 'select', true]
                        , 'filterText': 'Type to filter... '
                    })
                });
            }}
        ]
    );

    print HTM center(a({-href => '/bs.cgi'},'CM Build Status'));
    print HTM start_table({-id => 'main-table', -border =>1, -cellpadding => 4});
    print HTM "<div class='paging'> </div>";

    # add suffix to BW and Size header
    my @header =
        map {$_ eq 'BW'   ? 'BW (Mbps)' : $_}
        map {$_ eq 'Size' ? 'Size (GB)' : $_}
    @cols[@dispcol];

    print HTM TR(th([@header]));
    my $href_aws_fmt = join('', qw{
        https://console.aws.amazon.com/ec2/v2/home?region=us-east-1
        #Images:visibility=owned-by-me;imageId=%s;sort=Build
    });

    my $href_proj_fmt = join('', qw{
        /bs.cgi?proj=%s
    });

    my $href_bnum_fmt = join('', qw{
        /bs.cgi?proj=%s
        &min_bnum=%s.0
        &max_bnum=%s.9999
    });

    my $count = 0;
    while (my $rs = $sth->fetch()) {
        $row{Built} =~ s/:00$//; # remove seconds
        #D say Dumper \%row;
        $row{ID} = a({-href => sprintf($href_aws_fmt,$row{ID})}, $row{ID});
        my ($proj, $bnum) = @row{qw(Project Build)};
        unless ($proj) {
            $proj = lc join('-',@row{qw(Product Release HF)});
            $proj =~ s/-$//;
            $proj =~ s/-//;
        }
        $row{Project} = a({-href => sprintf($href_proj_fmt,$proj)}, $proj);
        $row{Build} = a({-href => sprintf($href_bnum_fmt,$proj,($bnum) x 2)}, $bnum);
        # remove suffix and express BW in terms of Mbps
        $row{BW} =~ s/[MG]bps//;
        $row{BW} = 1000 if 1 == $row{BW};
        $row{Size} =~ s/GB//;
        printf HTM "%s\n", TR(td([@row{@cols[@dispcol]}]));
        $count++;
    }

    print HTM end_table();
    print HTM "<div class='paging'> </div>";
    printf HTM center(p("Generated on %s by %s:%s")), $a = localtime(time()), hostname(), $0;
    printf HTM center(a({-href => 'http://www.badgerbadgerbadger.com'}, "Badger Badger"));
    print HTM end_html();

    return 0;
}

=pod

=head1 NAME

 amilist - lists AMIs in an html page

=head1 SYNOPSIS

 perl amilist.pl

=head1 DESCRIPTION

 Lists AMIs found in amipub.db and produces amilist.html in the current directory.

=head1 SEE ALSO

 amicln.pl
 amicsv.pl
 amidesc.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
