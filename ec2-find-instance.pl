# displays running instances
#
use strict;
use Data::Dumper;
use VM::EC2;
use YAML;

exit main();

sub main
{
    $Data::Dumper::Indent = 1;
    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');

    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY},-secret_key=>$aws->{AWS_SECRET_KEY});

    my @instances;
    @instances = $ec2->describe_instances();

    open DBG,">dbg.txt";
    print DBG Dumper \@instances;

    my @ioi = grep { # instances of interest
        $_->{data}{keyName} eq $aws->{AWS_KEYPAIR_NAME}
    } @instances;

    for (@ioi) {
        my $data = $_->{data};
        my $zone = $data->{placement}{availabilityZone};
        my $name = $data->{tagSet}{item}{Name}{value};
        printf "%s\n", join(" "
            , $name, $zone, @{$data}{qw(imageId instanceType keyName instanceId launchTime)}
        );
    }
    return 0;
}
