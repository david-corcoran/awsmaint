#! tags AMIs with expiry tag
use strict;
use Data::Dumper;
use feature qw(say);

use VM::EC2;
use HTTP::Date qw(parse_date);
use CGI qw(:standard :html4);
use YAML;
use DBI;

exit main();

sub main
{
    my $dbg = 1;
    open X,">x";
    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});
    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY},-secret_key=>$aws->{AWS_SECRET_KEY});
    my $sth;

    my $sth = $dbh->prepare(qq{
        select date(a.seen,'+3 month'), a.imageId, ifnull(project,'') = 'tmos-tier2' tmos_tier2
        from firstseen a
        left join amiall b
        on a.imageId = b.imageId
        where ifnull(a.tagged,0) = 0
        limit 50
    });

    my %row;
    $sth->bind_columns(\@row{qw(expiry imageId tmos_tier2)});
    
    $sth->execute();

# for deleting tags I created earlier in the life of the project
# can use this as a sample (see delte_tags below)
#    my %tags = (
#        Build => undef
#        , License => undef
#        , HF => undef
#        , Release => undef
#        , Size => undef
#        , BW => undef
#        , Product => undef
#        , official => undef
#        , Grade => undef
#        , Built => undef
#        , Project => undef
#    );

    my @amis;
    while ($sth->fetch()) {
        my $ami;
        
        if ($ami = $ec2->describe_images($row{imageId})) {
            # $ami->delete_tags(\%tags);
            $ami->add_tags({expiry => $row{tmos_tier2} ? 'none' : $row{expiry}});
            say join ' ', $ami, $row{tmos_tier2} ? 'none' : $row{expiry};
            push @amis, $row{imageId};
        }
    }

    $sth = $dbh->prepare(qq{
        update firstseen
        set tagged = 1
        where imageId = ?
    });

    for my $ami (@amis) {
        $sth->execute($ami);
    }

    return 0;
}

=pod

=head1 NAME

 amitag - tags AMIs in AWS with an expiry tag

=head1 SYNOPSIS

 perl amitag.pl

=head1 DESCRIPTION

Set an expiry tag on AMIs in AWS based on the date an AMI was first seen (table firstseen). The 
expiry is set to 3 months from the date when the AMI was first seen, when running amipop.pl.

=head1 SEE ALSO

 amicln.pl
 amicsv.pl
 amidesc.pl
 amilist.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
