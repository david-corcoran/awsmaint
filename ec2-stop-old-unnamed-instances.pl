#!/usr/bin/perl
# interrogate AWS and stop running instances that are unamed and launched over 3 days ago
#
use strict;
use Data::Dumper;
use VM::EC2;
use YAML;
use feature qw(say);

exit main();

sub main
{
    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY},-secret_key=>$aws->{AWS_SECRET_KEY});
    my $verbose = 1 if $ARGV[0] eq '-v';

    my @date = (localtime(time()-3600*24*3))[5,4,3]; 
    $date[0]+=1900;
    $date[1]+=1;
    my $datemark = sprintf "%04d-%02d-%02d", @date; 
    my @instances =  $ec2->describe_instances();
    my @uinstances = grep {
        length($_->{data}{tagSet}{item}{Name}{value}) == 0
        and $_->{data}{instanceState}{name} eq 'running'
        and $_->{data}{launchTime} le $datemark
        and $_->{data}{keyName} eq $aws->{AWS_KEYPAIR_NAME}
    } @instances;

    if (@uinstances) {
        say "Deleting:";
        for my $ui (@uinstances) {
            printf "%s %s %s %s\n"
            , $ui->{data}{instanceId}
            , $ui->{data}{launchTime}
            , $ui->{data}{instanceState}{name}
            , $ui->{data}{tagSet}{item}{Name}{value} if $verbose;
            $ui->stop();
        }
    } else {
        say "No old unnamed instances running" if $verbose; 
    }
    return 0;
}
