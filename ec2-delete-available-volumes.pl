# ec2 script to delete all 'available' volumes, i.e. those not 'in-use'
use strict;
use Data::Dumper;
use VM::EC2;
use YAML;

exit main();

sub main
{
    my $awsinfo = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $ec2 = VM::EC2->new(-access_key=>$awsinfo->{AWS_ACCESS_KEY},-secret_key=>$awsinfo->{AWS_SECRET_KEY});
    my $ec2 = VM::EC2->new();
    my @volumes =  $ec2->describe_volumes(-filter => {status=>'available'});
    #D
    print Dumper \@volumes; exit();

    my $count = $ARGV[0];
    my $k = 0;
    for my $vol (@volumes) {
        printf "%s %s\n", $vol,$vol->{data}{size};
        # need to check date first $ec2->delete_volume($vol) if $ARGV[0];
        printf "%s\n", $ec2->error() if $ec2->is_error();
        $k++;
        last if --$count <= 0;
    }

    printf "Volumnes deleted: %d\n", $k;
    return 0;
}

