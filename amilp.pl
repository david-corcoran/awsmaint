# get launch permission

use strict;
use Data::Dumper;
use VM::EC2;
use DBI;
use YAML;
use feature qw(say);

exit main();

sub main
{
    my $verbose =1 if $ARGV[0] eq '-v';
    $Data::Dumper::Indent = 1;
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});
    my %sth;

    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY},-secret_key=>$aws->{AWS_SECRET_KEY});

    update_counts($dbh);

    # get all ids where no perm count exists
    # or ami perms that haven't been checked recently
    # limit the AMIs to interrogate ($limit) by number
    # and age
    #
    my $days_ago = 7;
    my $limit    = 5;
    my @amis = map {$_->[0]} @{$dbh->selectall_arrayref(qq{
        select imageId
        from (
            select imageId
            from amiall
            where LPcnt is null
            union
            select imageId
            from lpchktime
            where chktime <= datetime('now','localtime','-$days_ago day')
        )
        limit $limit
    })};

    $sth{ins} = $dbh->prepare(qq{
        insert into launchperm(userId, imageId)
        values (?,?)
    });

    for my $i (@amis) {
        my $ami = $ec2->describe_images($i);
        unless ($ami) {
            warn "ami not found: $i";
            next;
        }
        my @perms = $ami->launchPermissions();
        printf "%s\n", $ami if $verbose;
        if (@perms) {
            for my $p (@perms) {
                $sth{ins}->execute($p,$i);
                printf "  %s\n", $p if $verbose;
            }
        } else { # if none set a check time so we don't incessantly trouble AWS
            $sth{ins}->execute(0,$i); # userId 0 denotes 'none'
        }
        # printf "%s\n", Dumper \@perms;
    }

    update_counts($dbh); # again

    # clean up
    # if an image has 0 for perms but also other ids then delete the ones with 0,
    # this condition arises when an instance get its first launch permission assigment
    #
    $dbh->do(qq{
        delete from launchperm
        where imageId in (
            select imageId
            from launchperm
            group by imageId
            having count(*) > 1
        )
        and userId = 0
    });

    return 0;
}

sub update_counts
{
    my ($dbh) = @_;

    # update main table with LP counts, so it is easier to handle
    #
    my $counts = $dbh->selectall_hashref(qq{
        select count(*) cnt,  imageId
        from launchperm
        where userId > 0
        group by imageId
        union
        select 0, imageId
        from launchperm
        where userId = 0
    }, 'imageId' );

    my $sth = $dbh->prepare(qq{
        update amiall
        set LPcnt = ?
        where imageId = ?
    });

    while (my ($id, $hash) = each %$counts) {
        $sth->execute($hash->{cnt}, $id);
    }
    #
    # at this point the amiall table has the counts as found in
    # launchperm
    return 0;
}

# list of images from local db (SQLite:amipub.db)
# and interrogate AWS for launch permission if a given images
# has not been queried ever, or in the last week
#
=pod

=head1 NAME

 amilp - get launch permissions

=head1 SYNOPSIS

 perl amilp.pl [-v]

=head1 DESCRIPTION

Interrogates AWS for launch permission for each AMI, tracks the time of the update and
populates the amiall table with the count for subsequent display. It is designed to
run periodically and it throttles itself so as not to cause undo querying of AWS. Specify -v
to see some output.

=head1 SEE ALSO

 amicln.pl
 amicsv.pl
 amidesc.pl
 amilist.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
