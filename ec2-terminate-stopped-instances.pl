# terminate AMI instances over 1 week old
# 
use strict;
use Data::Dumper;
use VM::EC2;
use YAML;
$Data::Dumper::Indent = 1;

exit main();

sub main
{
    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY},-secret_key=>$aws->{AWS_SECRET_KEY});

    my @x;
    @x = $ec2->describe_instances();

    my @y = grep {
        $_->{data}{keyName} eq $aws->{AWS_KEYPAIR_NAME}
        and $_->{data}{instanceState}{name} eq 'stopped'
        and not defined($_->{data}{tagSet}{item}{Name}{value}) 
        and $_->{data}{launchTime} lt '2015-04-30'
    } @x;

    #D printf "%s %s\n", $_->{data}{instanceId}, $_->{data}{launchTime} for @y; exit();

    # need to check date first so just print and exit

    my $count = $ARGV[0] =~ /^(\d+)$/ ? $1 : 1;

    my $j = 0;
    my $k = 0;
    for my $i (@y) {
        printf "%s %s\n",$i, $i->terminate();
        for my $b (@{$i->{data}{blockDeviceMapping}{item}}) {
            my $vol = $b->{ebs}{volumeId};
            if ($ec2->delete_volume($vol)) {
                printf "%s %s\n", $vol, 'deleted';
                $k++;
            }
        }
        $j++;
        last if --$count <= 0;
    }

    printf "Instances terminated: %d\n", $j;
    printf "Volumnes deleted: %d\n", $k;
}
