#! /usr/bin/perl
use strict;
use feature qw(say);
use VM::EC2;
use YAML;
use DBI;
use Data::Dumper;

exit main();

sub main
{
    my $awsinfo = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $cmuser  = YAML::LoadFile('/root/.mysql/cmuser-cookie.yml');

    my $connect = sprintf "dbi:mysql:host=%s:dbname=%s", @{$cmuser}{qw(host database)};
    my $dbh = DBI->connect($connect, @{$cmuser}{qw(user password)});

    $connect = "dbi:SQLite:dbname=amipub.db";
    my $dbh1 = DBI->connect($connect);
    say $dbh1; return 0;

    #D say $dbh; return 0;
    #D say Dumper $awsinfo; return 0;
    #D say Dumper $cmuser; return 0;

    my $ec2 = VM::EC2->new(
        -access_key=>$awsinfo->{AWS_ACCESS_KEY}
        , -secret_key=>$awsinfo->{AWS_SECRET_KEY}
    );

    #D say Dumper $awsinfo, $ec2; return 0;

    $a=-1;
    my $datemark = sprintf("%04d-%02d-%02d",  (map {
        ++$a == 4 ? $_+1
        : $a == 5 ? $_+1900
        : $_
    } (localtime(time()-3600*24*20)))[5,4,3]);
    say $datemark;
    my @instances = $ec2->describe_instances();
    my @uinstances = grep {
        1 # and length($_->{data}{tagSet}{item}{Name}{value}) == 0
        and $_->{data}{instanceState}{name} eq 'running'
        and $_->{data}{launchTime} le $datemark
        and $_->{data}{keyName} eq $awsinfo->{AWS_KEYPAIR_NAME};
    } @instances;
    print Dumper \@uinstances;
    return 0;
    # my @regions   = $ec2->describe_regions(); 
    # say Dumper @regions;
    return 0;
}
