use strict;
use Data::Dumper;
use CGI qw(:standard :html4 :start_table :end_table);
use DBI;


exit main();

# F5 Networks Hourly Hotfix-BIGIP-11.6.0.3.0.412-HF3 - Best 25Mbps - built on Feb 20 2015 10_10AM - size_104GB

sub main
{
    my $count = $ARGV[0] if $ARGV[0] =~ /^\d+$/;

    my $dbg = 1;
    open X,">x";
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});
    my $rev = '11.6';
    printf "\nhttps://docs.f5net.com/display/PDVADCTEAM/Setup+Of+Different+Platforms+for+VE\n", $rev;

    my $sth;
# imageId, Built, License, Type, Product, Release, Project, Build, Grade, BW, Size, HF, key, name
    $sth = $dbh->prepare(qq{
        select Built, 'CM' By, Project, imageId ID, name Description, 'N. Virginia' Region
        from amiall
        order by built desc
    });

# CVS colums + 4 blanks
# Built By Project ID Description Region '' '' '' ''

    my %row;
    my @cols = qw(Built By Project ID Description Region);
    $sth->bind_columns(\@row{@cols});
    $sth->execute();

    open CSV,">amilist.csv";

    while (my $rs = $sth->fetch()) {
        $row{Built} =~ s/:00$//; # remove seconds
        #D print Dumper \%row;
        printf CSV qq("%s"\n), join('","', @row{@cols},('') x 4);
        last if defined($count) and $count-- <= 1;
    }

    return 0;
}

=pod

=head1 NAME

 amicsv.pl - create a csv file suitable for pasting into confluence

=head1 SYNOPSIS

 perl amicsv.pl [n]

=head1 DESCRIPTION

Reads amipub.db and converts all the records, if n not specified (if specified
limits query to n), to CSV and writes to amilist.csv
in the current directory. This file can be opened in Excel and then cut 'n pasted into confluence
where they keep track of the AMI builds.


=head1 SEE ALSO

 amicln.pl
 amidesc.pl
 amilist.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
