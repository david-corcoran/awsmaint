#! /usr/bin/perl
# extract relevant info from descriptions in intake and insert into amiall
# for generating html ami list page
#
use strict;
use feature qw(say);
use Data::Dumper;
use HTTP::Date qw(parse_date);
use DBI;

exit main();

sub main
{
    my $dbg = 1;
    open X,">x";
  
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});

    # no provision for BIG-IQ yet :-(
    my @regexps = ( 
        {
            # Hourly Hotfix-BIGIP-11.6.0.4.0.420-HF4
            key => [qw(License Type Product Release Build HF)] 
            , re => qr/(Hourly|BYOL) (Hotfix)-(BIGIP)-(\d+\.\d+\.\d+)\.\d+\.\d+\.(\d+)-(HF\d+)/
        },{
            # Hourly BIGIP-11.5.2.0.0.141 
            key => [qw(License Product Release Build)] 
            , re => qr/(Hourly|BYOL) (BIGIP)-(\d+\.\d+\.\d+)\.\d+\.\d+\.(\d+)/
        },{
            # BYOL Hotfix-BIGIP-11.4.0-2464.0-HF10
            key => [qw(License Type Product Release Build HF)] 
            , re => qr/(Hourly|BYOL) (Hotfix)-(BIGIP)-(\d+\.\d+\.\d+)-(\d+\.\d)-(HF\d+)/
        },{
            key => [qw(License Product Project Release Build)]
            # BYOL BIGIP-tmos-bugs-staging-12.0.0.0.0.1541
            , re => qr/(Hourly|BYOL) (BIGIP)-([-a-zA-Z0-9]+)-(\d+\.\d+\.\d+).\d+\.\d+\.(\d+)/
        }
    );

    # determine which AMIs need to be updated
    #
    my %amis = map {$_->[0], $_->[1]} @{$dbh->selectall_arrayref(qq{
        select imageId, name
        from intake
        where imageId in (
            select imageId
            from intake
            except
            select imageId
            from amiall
        )
    })};

    my $sth = $dbh->prepare(qq{
        insert into amiall(imageId, Built, License, Type, Product 
           , Release, Project, Build, Grade, BW, Size, HF, key, name)
        values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    });

    my %data;
    $dbh->do('begin transaction');
    while (my ($id, $name) = each %amis) {
        $_ = $name;
        say $name;
        next unless s/F5 Networks //;
        next unless /BIGIP/; # does not yet handle BIG-IQ
        chomp;
        @data{qw(ld gr_bw bo Size)}  = split(/ - /);
        $data{Size} =~ s/size_//;
        $data{bo} =~ /built on (\w+\s+\d+\s+\d+)\s+(\d+)_(\d+[AP]M)/;
        my $date = sprintf "%04d-%02d-%02d",(parse_date($1))[0,1,2] ;
        my $time = sprintf "%.0s%s", split(/\s+/,parse_date("01-01-70 $2:$3")); 
        $data{Built} = "$date $time";
        @data{qw(Grade BW)} = split(/ /, $data{gr_bw});
        my %line;
        my $found;
        for my $re (@regexps) {
            if ($data{ld} =~ /$re->{re}/) {
                @line{@{$re->{key}}} = ($1,$2,$3,$4,$5,$6,$7);
                # print Dumper \%line;# if $line{hf} eq '';
                $found = 1;
                last;
            }
        }
        #
        printf "%s\n", $_ unless $found;
        my %record = (%data, %line, imageId => $id, name => $name);
        # build a key to distinguish same build done at different times
        $sth->execute(@record{qw(imageId Built License Type Product Release Project Build Grade BW Size HF key name)});
        print X Dumper $_,\%record;# unless $found; 
    }
    $dbh->do('end transaction');
    return 0;
}

=pod

=head1 NAME

 amidesc - extract build info from build descriptions

=head1 SYNOPSIS

 perl amidesc.pl

=head1 DESCRIPTION

Extracts the build information from description (name) of each AMI and populates the associated
fields in each record. It recognizes descriptions (after F5 Networks) of these four forms:

 F5 Networks Hourly Hotfix-BIGIP-11.6.0.4.0.420-HF4
 F5 Networks Hourly BIGIP-11.5.2.0.0.141 
 F5 Networks BYOL Hotfix-BIGIP-11.4.0-2464.0-HF10
 F5 Networks BYOL BIGIP-tmos-bugs-staging-12.0.0.0.0.1541

Notes:
 it does not recognize BIG-IQ builds
 any text prefacing F5 Networks is blithely accepted

=head1 SEE ALSO

 amicln.pl
 amicsv.pl
 amilist.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
