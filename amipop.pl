#!  /usr/bin/perl
# interrogate AWS for AMI descriptions and populate intake table in amipub.db
#
use strict;
use Data::Dumper;
use VM::EC2;
use HTTP::Date qw(parse_date);
use CGI qw(:standard :html4);
use DBI;
use YAML;
use feature qw(say);

exit main();

sub main
{
    my $dbg = 1;
    $DB::Single = $dbg;
    open TXT,">amis.txt";
    my $dbh = DBI->connect('dbi:SQLite:dbname=amipub.db','','',{RaiseError => 1});
    my $sth;

    # mark records so we can see which have been refreshed, those
    # that are not, are deemed to have been deleted in AWS
    #
    $dbh->do(qq{update intake set refreshed = null});
    #
    # unique constraint replaces record which will have the current
    # date in the field

    my $aws = YAML::LoadFile('/root/.aws/awsinfo.yml');
    my $ec2 = VM::EC2->new(-access_key=>$aws->{AWS_ACCESS_KEY}, -secret_key=>$aws->{AWS_SECRET_KEY});
    my @amis = $ec2->describe_images(-owner => $aws->{AWS_MY_ACCOUNT_ID});
    say TXT Dumper \@amis;

    # only interested in AMIs with F5 Networks in the title (name)
    my @aoi = @amis;
#    my @aoi = grep {
#        $_->{data}{name} =~ /F5 Networks/
#    } @amis;

    $sth = $dbh->prepare(qq{
        insert into intake(refreshed,isPublic, imageId, architecture, imageState, name)
        values(datetime('now','localtime'),?,?,?,?,?)
    });

    $dbh->do('begin transaction');
    my $data;
    for (@aoi) {
        $data = $_->{data};
        printf "%s\n", $data->{imageId};
        $sth->execute(@{$data}{qw(isPublic imageId architecture imageState name)});
    }
    $dbh->do(qq(delete from intake where refreshed is null));
    $dbh->do('end transaction');
    return 0;
}

=pod

=head1 NAME amipop.pl

 amipop.pl - populate the intake table for further processing

=head1 SYNOPSIS

 perl amipop.pl

=head1 DESCRIPTION

Interrogates AWS

=head1 SEE ALSO

 amicln.pl
 amicsv.pl
 amidesc.pl
 amilist.pl
 amilp.pl
 amipop.pl
 amitag.pl

=head1 FILES

 /root/.aws/awsinfo.yml - contains AWS keys to access CM's account
 ./amipub.db - the SQLite database with AMI info

=cut
